// DEBUGGING
// cd /Users/alex/Dev/NodeJS/homebridge
// git pull
// npm install
// npm run build
// Accessory konfigurieren unter ~/.homebridge/config.json
// DEBUG=* ./bin/homebridge -D -P ../homebridge-mqtt-endlesscube/


var Service, Characteristic;

/**
 * @module homebridge
 * @param {object} homebridge Export functions required to create a
 *                            new instance of this plugin.
 */
module.exports = function (homebridge) {
    console.log("homebridge API version: " + homebridge.version);

    Service = homebridge.hap.Service;
    Characteristic = homebridge.hap.Characteristic;
    homebridge.registerAccessory('homebridge-mqtt-endlesscube', 'MQTT-ENDLESS-CUBE', MQTT_ENDLESS_CUBE);
};

/**
 * Parse the config and instantiate the object.
 *
 * @summary Constructor
 * @constructor
 * @param {function} log Logging function
 * @param {object} config Your configuration object
 */
function MQTT_ENDLESS_CUBE(log, config) {
    //create instance for one stripe to store state
    log("Config: " + JSON.stringify(config));
    var me = this;

    this.stripe = require("./mqtt-endlesscube-mod")(log, config, function (data) {
        if (data) {
            var i;
            for (i = 0; i < 3; i++) {
                me.lightbulbServices[i].getCharacteristic(Characteristic.On).setValue(data.sides[i].power, undefined, 'myListener');
                me.lightbulbServices[i].getCharacteristic(Characteristic.Brightness).setValue(data.sides[i].brightness, undefined, 'myListener');
                me.lightbulbServices[i].getCharacteristic(Characteristic.Hue).setValue(data.sides[i].hue, undefined, 'myListener');
                me.lightbulbServices[i].getCharacteristic(Characteristic.Saturation).setValue(data.sides[i].saturation, undefined, 'myListener');
            }
        }
        this.debug("listerner: state changed " + JSON.stringify(data))
    });

    this.log = log;
    this.name = config.name;
    this.debug = function (msg) {
        if (config.debug) {
            log("[DEBUG]" + msg);
        }
    }
}

/**
 * @augments MQTT_ENDLESS_CUBE
 */
MQTT_ENDLESS_CUBE.prototype = {

    /** Required Functions **/
    identify: function (callback) {
        this.log('Identify requested!');
        //TODO - blink a litte bit
        callback();
    },

    getServices: function () {
        // You may OPTIONALLY define an information service if you wish to override
        // default values for devices like serial number, model, etc.
        var informationService = new Service.AccessoryInformation();

        informationService
            .setCharacteristic(Characteristic.Manufacturer, 'MQTT ENDLESS CUBE')
            .setCharacteristic(Characteristic.Model, 'MQTT ENDLESS CUBE')
            .setCharacteristic(Characteristic.SerialNumber, 'MQTT ENDLESS CUBE 1');

        this.log('creating Lightbulb service');

        this.lightbulbServices = [
            this.registerLightBulbService(0, "A"),
            this.registerLightBulbService(1, "B"),
            this.registerLightBulbService(2, "C"),
        ];

        return [informationService, this.lightbulbServices[0], this.lightbulbServices[1], this.lightbulbServices[2]];
    },

    registerLightBulbService: function (sideIndex, sideName) {
        var lightbulbService = new Service.Lightbulb("Cube " + " " + sideName, sideName);

        // Handle on/off
        lightbulbService
            .getCharacteristic(Characteristic.On)
            .on('get', callback => this.getPowerState(sideIndex, callback))
            .on('set', (state, callback, origin) => this.setPowerState(sideIndex, state, callback, origin));

        // Handle brightness
        this.log('... adding Brightness');
        lightbulbService
            .addCharacteristic(new Characteristic.Brightness())
            .on('get', callback => this.getBrightness(sideIndex, callback))
            .on('set', (state, callback, origin) => this.setBrightness(sideIndex, state, callback, origin));

        // Handle color
        this.log('... adding Hue');
        lightbulbService
            .addCharacteristic(new Characteristic.Hue())
            .on('get', callback => this.getHue(sideIndex, callback))
            .on('set', (state, callback, origin) => this.setHue(sideIndex, state, callback, origin));

        this.log('... adding Saturation');
        lightbulbService
            .addCharacteristic(new Characteristic.Saturation())
            .on('get', callback => this.getSaturation(sideIndex, callback))
            .on('set', (state, callback, origin) => this.setSaturation(sideIndex, state, callback, origin));

        return lightbulbService;
    },

    /**
     * Gets power state of led stripe.
     * @param {function} homebridge-callback function(error, result)
     */
    getPowerState: function (sideIndex, callback) {
        var result = this.stripe.getPowerState(sideIndex);
        this.debug('... powerState: ' + result);
        callback(null, result);
    },

    /**
     * Gets power state of led stripe.
     * @param state true = on, false = off
     * @param {function} homebridge-callback function(error, result)
     */
    setPowerState: function (sideIndex, state, callback, origin) {
        if (origin == 'myListener') {
            callback(undefined, state);
            return;
        }
        var me = this;
        this.debug('... setting powerState to ' + state);
        this.stripe.setPowerState(state, sideIndex, function (success) {
            me.debug('... setting powerState success: ' + success);
            callback(undefined, state);
        });
    },

    /**
     * Gets brightness of led stripe.
     * @param {function} homebridge-callback function(error, level)
     */
    getBrightness: function (sideIndex, callback) {
        var result = this.stripe.getBrightness(sideIndex);
        this.debug('... brightness: ' + result);
        callback(null, result);
    },

    /**
     * Sets brightness of led stripe.
     * @param {number} level 0-100
     * @param {function} callback The callback that handles the response.
     */
    setBrightness: function (sideIndex, level, callback, origin) {
        if (origin == 'myListener') {
            callback(undefined, level);
            return;
        }
        var me = this;
        this.debug('... setting brightness to ' + level);
        this.stripe.setBrightness(level, sideIndex, function (success) {
            me.debug('... setting brightness success: ' + success);
            callback(undefined, success)
        });
    },

    /**
     * Gets hue of led stripe.
     * @param {function} homebridge-callback function(error, level)
     */
    getHue: function (sideIndex, callback) {
        var result = this.stripe.getHue(sideIndex);
        this.debug('... hue: ' + result);
        callback(null, result);
    },

    /**
     * Sets hue of led stripe.
     * @param {number} level 0-360
     * @param {function} callback The callback that handles the response.
     */
    setHue: function (sideIndex, level, callback, origin) {
        if (origin == 'myListener') {
            callback(undefined, level);
            return;
        }
        var me = this;
        this.debug('... setting hue to ' + level);
        this.stripe.setHue(level, sideIndex, function (success) {
            me.debug('... setting hue success: ' + success);
            callback(undefined, success)
        });
    },

    /**
     * Gets saturation of led stripe.
     * @param {function} homebridge-callback function(error, level)
     */
    getSaturation: function (sideIndex, callback) {
        var result = this.stripe.getSaturation(sideIndex);
        this.debug('... saturation: ' + result);
        callback(null, result);
    },

    /**
     * Sets saturation of led stripe.
     * @param {number} level 0-100
     * @param {function} callback The callback that handles the response.
     */
    setSaturation: function (sideIndex, level, callback, origin) {
        if (origin == 'myListener') {
            callback(undefined, level);
            return;
        }
        var me = this;
        this.debug('... setting saturation to ' + level);
        this.stripe.setSaturation(level, sideIndex, function (success) {
            me.debug('... setting saturation success: ' + success);
            callback(undefined, success)
        });
    }

};
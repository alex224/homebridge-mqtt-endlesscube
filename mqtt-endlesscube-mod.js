const mqtt = require('mqtt');
const colors = require('./colors-util');
const cube = require('./endlesscubesender/cube-generator');
const Rx = require('rxjs');
const RxOps = require('rxjs/operators');
const colorsUtil = require('./colors-util');

const MAX_BRIGHTNESS = 30; //von 100
const SIDES = 3;

function clean(input) { /*
	var buf = Buffer.alloc(input.length + 4);
	buf.write(input);
	buf.writeUInt32BE(0);
	return buf; */
	return input;
}

module.exports = function (log, config, stateChangedListener) {
	//console.log(JSON.stringify(config));

	var module = {};
	module.stateChangedListener = stateChangedListener || function () { };
	module.originalLog = typeof log === 'function' ? log : console.log;

	module.log = function (msg) { module.originalLog(msg); }
	module.debug = function (msg) { if (config.debug) { module.originalLog("[DEBUG] " + msg) } }

	module.lastPublishedTimeMillis = 0;

	module.publishSubject = new Rx.Subject();
	module.publishSubject.asObservable().pipe(
		RxOps.debounceTime(500),
	).subscribe(payload => {
		module.lastPublishedTimeMillis = new Date().getTime();
		module.publishCurrentStateToMQTT(payload);
	})

	var brokerUrl = getParam(log, config, 'broker.url', true, null);
	module.log("Connecting to mqtt broker: " + brokerUrl + "...");
	var client = mqtt.connect(brokerUrl);
	client.on('connect', function () {
		module.log("Connected to mqtt broker: " + brokerUrl + ".");
		var topic = getParam(log, config, 'topic', true, null);
		if (topic) {
			module.log("Registering on topic " + topic);
			client.subscribe(topic);
		}
	});

	client.on('message', function (topic, message) {
		module.log("Received message from mqtt broker for topic: " + topic + ": " + ('' + message).substr(0, 20) + '...');
		var stateTopic = getParam(log, config, 'topic', true, null);
		if (topic == stateTopic) {
			try {
				//nur den Status anpassen wenn das Plugin nicht selber den Status veränder hat
				//Grund: Rundungsfehler durch RGB-HSB Umwandlung und Max-Brightness-Beschränkung
				if (message && (new Date().getTime() - module.lastPublishedTimeMillis > 5000)) {
					module.state.sides[0].brightness = 55;
					module.parseStateFromImage("" + message);
					module.stateChangedListener(module.state);
				}
			} catch (err) {
				module.log(err);
				module.log("" + err + " - Invalid mqtt payload? " + message.toString());
			}
		}
	});

	module.config = config;
	module.state = {
		sides: [
			{ power: false, hue: 100, brightness: 50, saturation: 30 },
			{ power: false, hue: 100, brightness: 60, saturation: 20 },
			{ power: false, hue: 100, brightness: 70, saturation: 10 }
		]
	};

	module.parseStateFromImage = function (image) {
		const middleOfEdgeIndex = 5;
		if (image.length > 100) {
			const pixelA = cube.sideMap.get(cube.Side.A)[middleOfEdgeIndex];
			const pixelB = cube.sideMap.get(cube.Side.B)[middleOfEdgeIndex];
			const pixelC = cube.sideMap.get(cube.Side.C)[middleOfEdgeIndex];
			//module.debug("============" + pixelA + " - " + pixelB + " - " + pixelC);

			const hexA = image.substring(pixelA * 6, (pixelA + 1) * 6);
			const hexB = image.substring(pixelB * 6, (pixelB + 1) * 6);
			const hexC = image.substring(pixelC * 6, (pixelC + 1) * 6);
			module.debug("Hex colors from mqtt payload: " + hexA + " - " + hexB + " - " + hexC);

			const colorA = module.fixHsvValues(colorsUtil.rgb2hsb(colorsUtil.hexToRgb(hexA)));
			const colorB = module.fixHsvValues(colorsUtil.rgb2hsb(colorsUtil.hexToRgb(hexB)));
			const colorC = module.fixHsvValues(colorsUtil.rgb2hsb(colorsUtil.hexToRgb(hexC)));

			//console.log(colorA);
			//console.log(colorB);
			//console.log(colorC);

			toState = function (hsv) {
				return { power: true, hue: hsv.h, saturation: hsv.s, brightness: hsv.v };
			}
			module.state.sides[0] = toState(colorA);
			module.state.sides[1] = toState(colorB);
			module.state.sides[2] = toState(colorC);
		}
	}

	module.fixHsvValues = function (hsv) {
		return {
			h: Math.round(Math.min(360, hsv.h * 360)),
			s: Math.round(Math.min(100, hsv.s * 100)),
			v: Math.round(Math.min(100, hsv.v * (100.0 / MAX_BRIGHTNESS) * 100)),
		}
	}

	module.publishCurrentStateToMQTT = function (payload) {
		var topic = getParam(log, config, 'topic', true, null);
		if (topic) {
			client.publish(topic, payload, { retain: true }, function (err) {
				if (err) module.log(err);
				if (!err) {
					module.log(("New image published to cube via MQTT."))
				}
			});
		}
	}

	module.buildCurrentState = function (callback) {

		var i = 0; var hexColors = []; var rgbColors = [];
		for (i = 0; i < SIDES; i++) {
			const brightness = (module.state.sides[i].brightness / 100) * MAX_BRIGHTNESS;
			var rgb = colors.hsb2rgb(module.state.sides[i].hue, module.state.sides[i].saturation, brightness);
			if (!module.state.sides[i].power) {
				rgb = { r: 0, g: 0, b: 0 };
			}
			rgbColors.push(rgb);

			//console.log("RGB: " + JSON.stringify(rgb))
			hexColors.push(colors.rgbToHex(rgb));
		}
		module.debug("Updating state: " + rgbColors.map(rgb => JSON.stringify(rgb)).join(", "));

		let image4 = new cube.ColorCubeBuilder() //
			.side(cube.Side.A, hexColors[0]) //
			.side(cube.Side.B, hexColors[1]) //
			.side(cube.Side.C, hexColors[2]) //
			.side(cube.Side.D, hexColors[0]) //
			.side(cube.Side.E, hexColors[1]) //
			.side(cube.Side.F, hexColors[2]) //
			.build();

		//TODO IMAGE erstellen
		var payload = image4;

		module.publishSubject.next(payload)

		callback(true); //kein echter callback mehr weil der mqtt-request ge-debounced wird.

	}

	module.requestState = function (requestId) {
		//no request of state supported
		/*
		var topic = getParam(log, config, 'topics.stateRequest', true, null);
		if (topic) {
			client.publish(topic, clean('homebridge-' + requestId), null, function (err) { if (err) module.log(err); });
		}*/
	}

	module.getPowerState = function (sideIndex) {
		module.debug('getPowerState ' + sideIndex);
		return module.state.sides[sideIndex].power;
	}

	module.setPowerState = function (state, sideIndex, callback) {
		module.debug('setPowerState ' + sideIndex + ": " + state);

		var i;
		for (i = 0; i < SIDES; i++) {
			module.state.sides[i].power = state ? 1 : 0;
		}

		this.buildCurrentState(success => callback(success));

		//Durch die gleichzeitige Änderung aller On/Off-States hier noch den Status aktualisieren
		module.stateChangedListener(module.state);

	};

	module.getBrightness = function (sideIndex) {
		module.debug('getBrightness ' + sideIndex);
		return module.state.sides[sideIndex].brightness;
	}

	module.setBrightness = function (value, sideIndex, callback) {
		module.debug('setBrightness ' + sideIndex + ": " + value);
		//value is between 0 and 100 -> map to 0 to 255
		var byteValue = (value * 255.0 / 100.0).toFixed(0);
		var i;
		for (i = 0; i < SIDES; i++) {
			module.state.sides[i].brightness = value;
		}
		this.buildCurrentState(success => callback(success));

		//Durch die gleichzeitige Änderung aller Helligkeiten hier noch den Status aktualisieren
		module.stateChangedListener(module.state);
	};

	module.getHue = function (sideIndex) {
		module.debug('getHue ' + sideIndex);
		return module.state.sides[sideIndex].hue;
	}

	module.setHue = function (value, sideIndex, callback) {
		module.debug('setHue ' + sideIndex + ": " + value);
		module.state.sides[sideIndex].hue = value;
		this.buildCurrentState(success => callback(success));
	};

	module.getSaturation = function (sideIndex) {
		module.debug('getSaturation ' + sideIndex);
		return module.state.sides[sideIndex].saturation;
	}

	module.setSaturation = function (value, sideIndex, callback) {
		module.debug('setSaturation ' + sideIndex + ": " + value);
		module.state.sides[sideIndex].saturation = value;
		this.buildCurrentState(success => callback(success));
	};

	function decimalToHex(d, padding) {
		var hex = Number(d).toString(16);
		padding = typeof (padding) === "undefined" || padding === null ? padding = 2 : padding;
		while (hex.length < padding) {
			hex = "0" + hex;
		}
		return hex;
	}

	function fetchFromObject(obj, prop) {

		if (typeof obj === 'undefined') {
			return null;
		}

		var _index = prop.indexOf('.')
		if (_index > -1) {
			return fetchFromObject(obj[prop.substring(0, _index)], prop.substr(_index + 1));
		}

		return obj[prop];
	}

	function getParam(log, configObject, paramName, mandatory, defaultValue) {
		var value = fetchFromObject(configObject, paramName);
		if (typeof value === 'undefined' || value == null) {
			if (mandatory) {
				module.log("ERROR: Missing param " + paramName);
			}
			return defaultValue;
		} else {
			return value;
		}
	}

	return module;
};


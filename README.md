# homebridge-lw12-rgb-ledstrip

Homebridge-Plugin für den Endless-Cube

# Installation

1. Install homebridge using: npm install -g homebridge
2. Install this plugin using: npm install -g npm install git+https://alex224@bitbucket.org/alex224/homebridge-mqtt-endlesscube.git
3. Update your configuration file. See sample-config.json in this repository for a sample. 

# Requirement for opposite part - the led controller

- the controller supports one topic with a long string of hex values (6 bytes per pixel)

# Configuration

Configuration sample file:

 ```
	"accessories": [
		{
			"accessory": "MQTT-ENDLESS-CUBE",
			"name": "Unendlichkeitswuerfel",
			"broker": {
				"url": "mqtt://192.168.1.85"
			},
			"topic": "/home/device/endlesscube/image",
			"debug": false
		}
	]
```

# Debugging mit lokaler Homebridge Instanz

```
cd /Users/alex/Dev/NodeJS/homebridge
DEBUG=* ./bin/homebridge -D -P ../homebridge-mqtt-endlesscube/
oder
DEBUG=* node debug ./bin/homebridge -D -P ../homebridge-mqtt-endlesscube/
```

Danach kann man sich per VSCode Debugger attachen
`c` = Continue

# Development

Die Dateien im Verzeichnis `endlesscubesender` stammen aus dem `dist`-Verzeichnis des `EndlessCubeSender`-Projekts nach Ausführung von `tsc` bzw. vereinfacht `npm run start`. Darin sind also auch die korrekten Mappings der LEDs zu den Seiten/Kanten.


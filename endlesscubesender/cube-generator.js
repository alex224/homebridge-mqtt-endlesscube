"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NUMPIXELS = 12 /*Kanten*/ * 11 * 2 /* LEDS pro Kante */ - 1;
//Flächen von vorne ausgesehen. Augen auf Höhe der Spitze
// A: vorne oben links
// B: vorne oben rechts
// C: vorne unten
// D: hinten rechts unten (gegenüber A)
// E: hinten links unten (gegenüber B)
// F: hinten oben (gegenüber C)
const edge_11_E = /*  0 - 10 */ [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const edge_08_E = /* 11 - 21 */ [11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21];
const edge_12_E = /* 22 - 32 */ [22, 23, 24, 25, 26, 27, 28, 20, 30, 31, 32];
const edge_04_E = /* 33 - 43 */ [33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43];
const edge_01_C = /* 44 - 54 */ [44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54];
const edge_02_C = /* 55 - 65 */ [55, 56, 57, 59, 59, 60, 61, 62, 63, 64, 65];
const edge_03_C = /* 66 - 76 */ [66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76];
const edge_04_C = /* 77 - 87 */ [77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87];
const edge_11_D = /* 88 - 98 */ [88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98];
const edge_05_D = /* 99 - 109 */ [99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109];
const edge_10_D = /* 110 - 120 */ [110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120];
const edge_01_D = /* 121 - 131 */ [121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131];
const edge_06_B = /* 132 - 141 */ [/**/ , 132, 133, 134, 135, 136, 137, 138, 139, 140, 141]; // <-- hier fehlt die erste LED
const edge_10_B = /* 142 - 152 */ [142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152];
const edge_02_B = /* 152 - 163 */ [153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163];
const edge_09_B = /* 164 - 174 */ [164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174];
const edge_07_F = /* 175 - 185 */ [175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185];
const edge_08_F = /* 186 - 196 */ [186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196];
const edge_05_F = /* 197 - 207 */ [197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207];
const edge_06_F = /* 208 - 218 */ [208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218];
const edge_09_A = /* 219 - 229 */ [219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229];
const edge_03_A = /* 230 - 240 */ [230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240];
const edge_12_A = /* 241 - 251 */ [241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251];
const edge_07_A = /* 252 - 262 */ [252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262];
const sideA = edge_09_A.concat(edge_03_A).concat(edge_12_A).concat(edge_07_A);
const sideB = edge_06_B.concat(edge_10_B).concat(edge_02_B).concat(edge_09_B);
const sideC = edge_01_C.concat(edge_02_C).concat(edge_03_C).concat(edge_04_C);
const sideD = edge_11_D.concat(edge_05_D).concat(edge_10_D).concat(edge_01_D);
const sideE = edge_11_E.concat(edge_08_E).concat(edge_12_E).concat(edge_04_E);
const sideF = edge_07_F.concat(edge_08_F).concat(edge_05_F).concat(edge_06_F);
const all = sideA.concat(sideB).concat(sideC).concat(sideD).concat(sideE).concat(sideF);
var Side;
(function (Side) {
    Side[Side["A"] = 0] = "A";
    Side[Side["B"] = 1] = "B";
    Side[Side["C"] = 2] = "C";
    Side[Side["D"] = 3] = "D";
    Side[Side["E"] = 4] = "E";
    Side[Side["F"] = 5] = "F";
})(Side = exports.Side || (exports.Side = {}));
exports.sideMap = new Map();
exports.sideMap.set(Side.A, sideA);
exports.sideMap.set(Side.B, sideB);
exports.sideMap.set(Side.C, sideC);
exports.sideMap.set(Side.D, sideD);
exports.sideMap.set(Side.E, sideE);
exports.sideMap.set(Side.F, sideF);
class CubeGenerator {
    oneColor(hexRgbColor) {
        let result = '';
        for (let i = 0; i < exports.NUMPIXELS; i++) {
            result += hexRgbColor;
        }
        return result;
    }
    ledsFromIndex(hexRgbColor, fromIndex, toIndex) {
        let result = '';
        for (let i = 0; i < fromIndex; i++) {
            result += '000011';
        }
        for (let i = fromIndex; i < toIndex; i++) {
            result += hexRgbColor;
        }
        for (let i = toIndex; i < exports.NUMPIXELS; i++) {
            result += '110000';
        }
        return result;
    }
}
exports.CubeGenerator = CubeGenerator;
class ColorCubeBuilder {
    constructor() {
        this.pixelRGBs = [];
        for (var i = 0; i < exports.NUMPIXELS + 1; i++) {
            this.pixelRGBs.push('000000');
        }
    }
    colorizes(pixelIndexes, hexRgbColor) {
        for (let i = 0; i < pixelIndexes.length; i++) {
            this.pixelRGBs[pixelIndexes[i]] = hexRgbColor;
        }
        return this;
    }
    all(hexRgbColor) {
        this.colorizes(all, hexRgbColor);
        return this;
    }
    side(side, hexRgbColor) {
        this.colorizes(exports.sideMap.get(side), hexRgbColor);
        return this;
    }
    pixel(index, hexRgbColor) {
        //if (index > 0 && index < NUMPIXELS) {
        this.pixelRGBs[index] = hexRgbColor;
        //}
    }
    build() {
        let result = '';
        this.pixelRGBs //
            .map(pixel => pixel && pixel.length == 6 ? pixel : '000000') //
            .forEach(pixel => result += pixel);
        return result;
    }
}
exports.ColorCubeBuilder = ColorCubeBuilder;
class AnimationBuilder {
    constructor() {
        this.animations = [];
    }
    addImageWithDuration(image, duration) {
        this.animations.push({ image, duration });
        return this;
    }
    build() {
        let result = 'ANIM,' + this.animations.length;
        this.animations.forEach(anim => result += '\n\n>' + anim.duration + '~' + anim.image + '<');
        return result;
    }
}
exports.AnimationBuilder = AnimationBuilder;
//# sourceMappingURL=cube-generator.js.map
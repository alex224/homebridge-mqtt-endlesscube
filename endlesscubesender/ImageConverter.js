"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const pixels = require("image-pixels");
class ImageConverter {
    readPNG(filename, maxWidth = 15, maxHeight = 20) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log("Reading piskel file...");
            const { data, width, height } = yield pixels(filename);
            return this.convertPixelBufferToRGBHex(data, width, height, maxWidth, maxHeight);
        });
    }
    readPiskel(filename, maxWidth = 15, maxHeight = 20) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log("Reading piskel file...");
            const base64Png = this.readPNGFromPiskel(filename);
            const { data, width, height } = yield pixels(base64Png);
            return this.convertPixelBufferToRGBHex(data, width, height, maxWidth, maxHeight);
        });
    }
    readPNGContent(content, maxWidth = 15, maxHeight = 20) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log("Reading piskel file...");
            const { data, width, height } = yield pixels(new Uint8Array(content));
            return this.convertPixelBufferToRGBHex(data, width, height, maxWidth, maxHeight);
        });
    }
    readPixil(filename, maxWidth = 15, maxHeight = 20) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log("Reading pixil file...");
            const base64Png = this.readPNGFromPixil(filename);
            const { data, width, height } = yield pixels(base64Png);
            return this.convertPixelBufferToRGBHex(data, width, height, maxWidth, maxHeight);
        });
    }
    readPixilFileContent(content, maxWidth = 15, maxHeight = 20) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log("Reading pixil file...");
            var decodedString = String.fromCharCode.apply(null, new Uint8Array(content));
            const base64Png = this.readPNGBase64ContentFromPixilFileContent(decodedString);
            const { data, width, height } = yield pixels(base64Png);
            return this.convertPixelBufferToRGBHex(data, width, height, maxWidth, maxHeight);
        });
    }
    convertPixelBufferToRGBHex(data, width, height, maxWidth, maxHeight, maxBrightness = 0.4) {
        const maxBright = Math.max(0.0, Math.min(1.0, maxBrightness));
        let hexResult = "";
        for (let y = 0; y < height && y < maxHeight; y++) {
            for (let x = 0; x < width && x < maxWidth; x++) {
                const index = (y * width * 4) + x * 4;
                const red = data[index];
                const green = data[index + 1];
                const blue = data[index + 2];
                const alpha = data[index + 3];
                //console.log(`${x}:${y} - rgba(${red}, ${green}, ${blue}, ${alpha})`)
                const alphaFactor = alpha / 255.0;
                const resultBrightness = maxBrightness * alphaFactor;
                hexResult += this.toHexString(new Uint8Array([(resultBrightness * red), (resultBrightness * green), (resultBrightness * blue)]));
            }
        }
        return this.injectLineFeeds(hexResult, width * 6);
    }
    injectLineFeeds(str, everyChar) {
        let result = '';
        for (let i = 0; i < str.length; i += everyChar) {
            result += str.substr(i, everyChar) + '\r\n';
        }
        return result;
    }
    toHexString(byteArray) {
        return Array.from(byteArray, function (b) {
            return ('0' + (b & 0xFF).toString(16)).slice(-2);
        }).join('');
    }
    readPNGFromPixil(filename) {
        return this.readPNGBase64ContentFromPixilFileContent(fs_1.readFileSync(filename, 'utf8'));
    }
    readPNGBase64ContentFromPixilFileContent(pixilFileContent) {
        const pixil = JSON.parse(pixilFileContent);
        if (pixil && pixil.frames && pixil.frames.length > 0) {
            const frame = pixil.frames[0];
            if (frame && frame.layers && frame.layers.length > 0 && frame.layers[0].src) {
                const base64Fragment = frame.layers[0].src;
                const imageStartPos = base64Fragment.indexOf("iVBOR");
                if (imageStartPos > 0) {
                    return base64Fragment.substr(imageStartPos);
                }
            }
        }
        return null;
    }
    readPNGFromPiskel(filename) {
        const jsonContent = fs_1.readFileSync(filename, 'utf8');
        const piskel = JSON.parse(jsonContent);
        if (piskel && piskel.piskel && piskel.piskel.layers && piskel.piskel.layers.length > 0) {
            const layerOne = JSON.parse(piskel.piskel.layers[0]);
            if (layerOne && layerOne.chunks && layerOne.chunks.length > 0) {
                const imageBase64 = layerOne.chunks[0].base64PNG;
                const imageMarker = 'data:image/png;base64,';
                if (imageBase64.indexOf(imageMarker) == 0) {
                    const base64Png = imageBase64.substring(imageMarker.length);
                    return base64Png;
                }
            }
        }
        return null;
    }
}
exports.ImageConverter = ImageConverter;
//# sourceMappingURL=ImageConverter.js.map